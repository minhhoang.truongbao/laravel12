@extends('layout.main')

@section('main-title')
    Edit music
@endsection

@section('body')
    <div class="w-100 p-4">
        <form action="{{ route('musics.update', ['id' => $music->id]) }}" method="post" class="w-75 m-auto">
            @method('PUT')
            @include('musics.form')
        </form>
    </div>
@endsection
