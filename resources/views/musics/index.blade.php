@extends('layout.main')
@section('main-title')
    my music
@endsection

@section('body')
    <div class="w-100 p-4">
        @if (session()->has('action-music-success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('action-music-success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="mb-4 d-flex justify-content-end">
            <a href="{{ route('musics.create') }}" class="btn btn-primary">Create music</a>
        </div>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th class="text-center" scope="col">numbers</th>
                    <th class="text-center" scope="col">name</th>
                    <th class="text-center" scope="col">author</th>
                    <th class="text-center" scope="col">lyrics</th>
                    <th class="text-center" scope="col">desc</th>
                    <th class="text-center" scope="col">duration</th>
                    <th class="text-center" scope="col">action</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($musics as $music )
                    <tr>
                        <th class="text-center" scope="row">{{ $music->id }}</th>

                        <td class="text-center">{{ $music->name }}</td>
                        <td class="text-center">{{ $music->author }}</td>
                        <td class="text-center">{{ Illuminate\Support\Str::of($music->lyrics ?? '')->limit(30) }}</td>
                        <td class="text-center">{{ Illuminate\Support\Str::of($music->desc ?? '')->limit(30) }}</td>
                        <td class="text-center">{{ date('i:s', strtotime($music->duration)) }}</td>
                        <td class="d-flex justify-content-center">
                            <a class="btn btn-warning mx-1"
                                href="{{ route('musics.edit', ['id' => $music->id]) }}">Edit</a>
                            <button class="btn btn-danger mx-1" data-toggle="modal" data-target="#modalDelete{{ $music->id }}">Delete</button>
                            <!-- Modal -->
                            <div class="modal fade" id="modalDelete{{ $music->id }}" tabindex="-1"
                                aria-labelledby="modalLabel{{ $music->id }}" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="modalLabel{{ $music->id }}">Modal Confirm
                                                Delete</h5>
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            Are you sure?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">Close</button>
                                            <form action="{{ route('musics.destroy', ['id' => $music->id]) }}" method="post">
                                                @method('DELETE')
                                                @csrf
                                                <button type="submit" class="btn btn-primary">Ok</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>

                    </tr>
                @empty
                    <tr>
                        <td class="text-center" colspan="7">
                            NO DATA
                        </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
        @if ($musics->count())
            {{ $musics->onEachSide(1)->links() }}
        @endif
    </div>
@endsection
