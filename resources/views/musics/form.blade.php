@csrf
<div class="form-group">
    <label for="name">Name:</label>
    <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $music->name ?? '') }}">
    @error('name')
        <small class="form-text text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    <label for="author">Author:</label>
    <input type="text" class="form-control" id="author" name="author"
        value="{{ old('author', $music->author ?? '') }}">
    @error('author')
        <small class="form-text text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    <label for="lyrics">Lyrics:</label>
    <textarea class="form-control" id="lyrics" name="lyrics"
        rows="3">{{ old('lyrics', $music->lyrics ?? '') }}</textarea>
    @error('lyrics')
        <small class="form-text text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    <label for="desc">Desc:</label>
    <textarea class="form-control" id="desc" name="desc" rows="3">{{ old('desc', $music->desc ?? '') }}</textarea>
    @error('desc')
        <small class="form-text text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    <label for="duration">Duration:</label>
    <input type="text" class="form-control" id="duration" placeholder="mm:ss" name="duration"
        value="{{ old('duration', isset($music) ? date('i:s', strtotime($music->duration)) : '') }}">
    @error('duration')
        <small class="form-text text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    <button type="submit" class="btn btn-primary">Submit</button>
