@extends('layout.main')

@section('main-title')
    Create music
@endsection

@section('body')
    <div class="w-100 p-4">
        <form action="{{ route('musics.store') }}" method="post" class="w-75 m-auto">
            @include('musics.form')
        </form>
    </div>
@endsection
