@extends('layout.main')
@section('main-title')
    user
@endsection

@section('body')
    <div class="w-100 p-4">
        @if (session()->has('action-music-success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('action-music-success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <table class="table table-hover">
            <thead>
                <tr>
                    <th class="text-center" scope="col">numbers</th>
                    <th class="text-center" scope="col">name</th>
                    <th class="text-center" scope="col">role</th>
                    <th class="text-center" scope="col">action</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($listUser as $user )
                    <tr>
                        <th class="text-center" scope="row">{{ $user->id }}</th>

                        <td class="text-center">{{ $user->name }}</td>
                        <td class="text-center">{{ $user->getRoleNames() }}</td>
                        <td class="d-flex justify-content-center">
                            <a class="btn btn-warning mx-1"
                                href="{{ route('user.edit', ['id' => $user->id]) }}">Edit</a>                            
                        </td>

                    </tr>
                @empty
                    <tr>
                        <td class="text-center" colspan="7">
                            NO DATA
                        </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection
