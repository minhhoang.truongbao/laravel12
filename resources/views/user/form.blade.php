@csrf
<div class="form-group">
    <label for="name">Name:</label>
    <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $user->name ?? '') }}">
    @error('name')
    <small class="form-text text-danger">{{ $message }}</small>
    @enderror
</div>

<div class="form-group col-sm-12">
    <label for="userRole">User Role:</label>
    <div class="form-group border rounded pt-2">
        @if($listRole)
        @foreach($listRole as $role)
        <div class="ml-3 pb-2">
            <div class="form-check">
                <input class="form-check-input me-1" id='check{{$role->id}}' type="checkbox" name="permission_id[]" value="{{ $role->id }}" {{$user->hasRole($role) ? "checked" : ""}}>
                <label class="form-check-label" for="check{{$role->id}}">{{ ucfirst(str_replace('_', ' ' ,$role->name)) }}</label>
            </div>
        </div>
        @endforeach
        @endif
    </div>
</div>

<div class="form-group">
    <button type="submit" class="btn btn-primary">Submit</button>
