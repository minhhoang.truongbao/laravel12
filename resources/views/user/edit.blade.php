@extends('layout.main')

@section('main-title')
    Edit User
@endsection

@section('body')
    <div class="w-100 p-4">
        <form action="{{ route('user.update', ['id' => $user->id]) }}" method="post" class="w-75 m-auto">
            @method('PUT')
            @include('user.form')
        </form>
    </div>
@endsection
