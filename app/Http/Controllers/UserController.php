<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function index()
    {
        $listUser = User::all();
        return view('user.index', compact('listUser'));
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        $listRole = Role::all();

        return view('user.edit', compact('user', 'listRole'));
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->syncRoles($request['permission_id']);

        return redirect()->route('user.index')->with('action-music-success', 'Edit user success!');
    }
}
