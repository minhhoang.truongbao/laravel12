<?php

namespace App\Http\Controllers;

use App\Http\Requests\MusicRequest;
use App\Models\Music;

class MusicController extends Controller
{
    public function index()
    {
        $musics = Music::orderByDesc('created_at')->paginate(20);
        return view('musics.index', compact('musics'));
    }

    public function create()
    {
        return view('musics.create');
    }

    public function store(MusicRequest $request)
    {
        Music::create($request->only(['name', 'author', 'lyrics', 'desc', 'duration']));

        return redirect()->route('musics.index')->with('message', 'Create music success!');
    }

    public function edit($id)
    {
        $music = Music::findOrFail($id);

        return view('musics.edit', compact('music'));
    }

    public function update(MusicRequest $request, $id)
    {
        $music = Music::findOrFail($id);
        $music->update($request->only(['name', 'author', 'lyrics', 'desc', 'duration']));

        return redirect()->route('musics.index')->with('action-music-success', 'Edit music success!');
    }

    public function destroy($id)
    {
        $music = Music::findOrFail($id);
        $music->delete();

        return redirect()->route('musics.index')->with('action-music-success', 'Delete music success!');
    }
}
