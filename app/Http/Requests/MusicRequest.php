<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MusicRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'bail',
                'required',
                'string',
                'max:255'
            ],
            'author' => [
                'bail',
                'required',
                'string',
                'max:255'
            ],
            'lyrics' => [
                'bail',
                'required',
                'string',
                'max:1000'
            ],
            'desc' => [
                'bail',
                'required',
                'string',
                'max:1000'
            ],
            'duration' => [
                'bail',
                'required',
                'date_format:i:s'
            ],
        ];
    }
}
