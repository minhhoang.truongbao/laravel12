<?php

use App\Http\Controllers\MusicController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['prefix' => 'musics', 'middleware' => ['auth']],function () {
    Route::get('/', [MusicController::class, 'index'])->name('musics.index');

    Route::get('create', [MusicController::class, 'create'])->name('musics.create')->middleware('checkeditor');

    Route::post('store', [MusicController::class, 'store'])->name('musics.store');

    Route::get('{id}/edit', [MusicController::class, 'edit'])->name('musics.edit')->middleware('checkeditor');

    Route::put('{id}/update', [MusicController::class, 'update'])->name('musics.update');

    Route::delete('{id}/destroy', [MusicController::class, 'destroy'])->name('musics.destroy')->middleware('checkeditor');
});

Route::group(['prefix' => 'user', 'middleware' => ['auth']],function () {
    Route::get('/', [UserController::class, 'index'])->name('user.index');

    Route::get('{id}/edit', [UserController::class, 'edit'])->name('user.edit')->middleware('checkadmin');

    Route::put('{id}/update', [UserController::class, 'update'])->name('user.update');
});

