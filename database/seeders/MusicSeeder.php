<?php

namespace Database\Seeders;

use App\Models\Music;
use Faker\Provider\ar_EG\Text;
use Illuminate\Database\Seeder;

class MusicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 100; $i++) {
            Music::create([
                'name' => 'Ten ca si',
                'author' => 'Ten tac gia',
                'desc' => 'Mo ta',
                'lyrics' => 'Loi bai hat',
                'duration' => '10:28',
            ]);
        }
    }
}
